$(function() {
  var $window = $(window);
  var windowWidth = $window.width();
  var windowSm = 768;
  var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);

  $window.load(function() {
    $('body').addClass("loaded");
  });

  $(".g-nav a[href='" + pgurl + "']").addClass("active");

  // PC Only
  if (windowWidth > windowSm) {
      var $fixedHeader = $(".header-fixed");
      var topContent = $(".section").offset().top;

      $window.on("scroll", function () {
        if ($window.scrollTop() > topContent) {
           $fixedHeader.addClass("is-open-header-fixed");
        } else {
           $fixedHeader.removeClass("is-open-header-fixed");
        }
      });
      $window.trigger("scroll");
  }

  var timer = false;
  $window.resize(function() {

      if (timer !== false) {
        clearTimeout(timer);
      }

      timer = setTimeout(function() {
        var windowWidth = $window.width();
        var windowSm = 768;
        if (windowWidth > windowSm) {
            if ($(".overlay-scale").hasClass("open")) {
              $(".overlay-scale").removeClass('open');
              $(".sp-nav-btn-icon").removeClass("close");
            }
        }
      }, 200);
  });

  $(".sp-nav-btn-block").click(function() {
      $(".overlay-scale").toggleClass("open");
      $(".sp-nav-btn-icon").toggleClass("close");
      return false;
  });

  $(".service-links li").click(function() {
    var target = $(this).data("target");
    $('.visual-service-' + target).velocity("scroll", { duration: 800, offset: -75, easing: "easeInOutQuart" });
  });

  $('.pagetop').click(function () {
    $('body, html').velocity("scroll", { duration: 1200, easing: "easeInOutQuart" });
    return false;
  });
});