Clanweiss Web
====

appフォルダ以下の
htmlファイルと、img/css/font/jsフォルダ（＋中身）があればそのまま表示可能です。  
このリポジトリにpushされた段階で、下記URLでブラウザ確認できます。  
[http://megane_ski.bitbucket.org/app/](http://megane_ski.bitbucket.org/app/)

開発時にはgulpを使用すると便利になります。

## モジュール

### Sass

cssの記述を便利にしてくれます。コンパイルでcssを出力します。

### Pug (旧Jade)

htmlを簡易的に記述することが出来るテンプレートエンジン。
変数やインクルードが使えます。
コンパイルでhtmlを出力します。

### Gulp

タスクランナーです。sassやpugのコンパイルを自動化したり、
ファイルを監視してリアルタイムでブラウザ更新してくれます。

## Usage / 使い方

node.jsがインストールされていることが前提です。

### npmパッケージのインストール

```
$ npm install
```

### gulpの実行

```
$ gulp
```

## pugファイル

* _header.pug（共通ヘッダー）
* _footer.pug（共通フッター）
* _layout.pug（共通レイアウト）
* _inc_meta_fb.pug（facebook用のmetaタグ出力用）
* index.pug等（アンダーバー無しはそのままhtmlとして出力。各ページの中身を書く）

### 各ページpugの中身

```
// ▼共通レイアウトの読み込み
extends _layout
block var
  - metas = {}
    // ▼以下各ページのfacebook用metaタグ設定
  - metas.title = 'title'
  - metas.url = 'url'
  - metas.image = 'image'
  - metas.site_name = 'site_name'
  - metas.description = 'description'
block append meta
  +meta(metas)
  include _inc_meta_fb
  +inc_meta_facebook(metas)
block title
　　// ▼titleタグに出力する文字列
  title クラヴィスについて | 株式会社クラヴィス 北海道札幌のWEB制作会社・システム開発会社
block link
  // ▼各ページ用リンクタグがある場合に記述
block head_script
  // ▼headタグ内にscriptを追加する場合に記述
block body
　　// ▼以下ページのコンテンツ部分
  main
  ... etc
block end_of_body
  // ▼bodyのとじタグの前にscriptを追加したりする場合に記述
```
