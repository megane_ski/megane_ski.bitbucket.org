'use strict';

var gulp = require('gulp');

var sass = require('gulp-sass');
var csscomb = require('gulp-csscomb');
var autoprefixer = require("gulp-autoprefixer");
require('es6-promise').polyfill();

var pug = require('gulp-pug');

var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');

var paths = {
  'scss': 'app/sass/**',
  'css': 'app/css',
  'pug': 'app/pug/**',
  'html': 'app'
}
var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
];

gulp.task('sass', function() {
  gulp.src(paths.scss + '/*.scss')
    .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
    .pipe(sass({style: 'expanded'}))
    .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
    .pipe(csscomb())
    .pipe(gulp.dest(paths.css));
});

gulp.task('pug', function () {
  gulp.src([paths.pug + '/*.pug',
    '!' + paths.pug +'/_*.pug'])
    .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(paths.html))
});

gulp.task("browserSync", function () {
    browserSync({
        server: {
            baseDir: "app" // ルートとなるディレクトリを指定
        }
    });
    gulp.watch("app/**/*.html", ['liveReload']);
    gulp.watch("app/**/*.css",  ['liveReload']);
    gulp.watch("app/**/*.js",   ['liveReload']);
});

gulp.task('liveReload', function() {
    browserSync.reload();
});

// watch
gulp.task('watch', function() {
  gulp.watch(paths.scss + '/*.scss', ['sass']);
  gulp.watch(paths.pug + '/*.pug', ['pug']);

})

gulp.task('default', ['sass', 'browserSync', 'pug', 'watch']);
